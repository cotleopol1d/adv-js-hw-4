"use strict";
const root = document.querySelector("#root");
const loadAnimation = document.querySelector(".lds-roller");
const url = `https://ajax.test-danit.com/api/swapi/`;

class Films {
    constructor(root, url, loadAnimation) {
        this.root = root;
        this.url = `${url}films`;
        this.loadAnimation = loadAnimation;
    }

    request() {
        return fetch(this.url, {
            headers: {
                "content-type": "application/json",
            }
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error(response.status);
                } else {
                    return response.json();
                }
            })
            .catch(e => {
                const errorText = document.createElement("p");
                errorText.textContent = e.message;
                this.root.append(errorText);
            })
    }

    render() {
        this.request()
            .then((data => {
                const filmList = document.createElement("ul");
                const filmItems = data.map(({episodeId, name, openingCrawl, characters}) => {
                    const filmItem = document.createElement("li");
                    if (name) {
                        const filmName = document.createElement("p");
                        filmName.textContent = `name: ${name}`;
                        filmItem.append(filmName);
                    }
                    if (episodeId) {
                        const filmEpisode = document.createElement("p");
                        filmEpisode.textContent = `episode number: ${episodeId}`;
                        filmItem.append(filmEpisode);
                    }
                    if (openingCrawl) {
                        const filmOpeningCrawl = document.createElement("p");
                        filmOpeningCrawl.textContent = `short content: ${openingCrawl}`;
                        filmItem.append(filmOpeningCrawl);
                    }
                    if (characters) {
                        const person = document.createElement("p");
                        person.textContent = "persons name:";
                        const personsList = document.createElement("ol");
                        const newAnimation = this.loadAnimation.cloneNode(true);
                        filmItem.append(person);
                        filmItem.append(personsList);
                        filmItem.append(newAnimation);
                        newAnimation.classList.remove("hidden");
                        new Persons(root, url, personsList, characters, newAnimation).render();
                    }
                    if (!filmItem.hasChildNodes()) {
                        filmItem.textContent = "Missing information about the film, try again later";
                    }
                    return filmItem;
                });
                filmList.append(...filmItems);
                this.root.append(filmList);

            }));
    }
}

class Persons {
    constructor(root, url, personsList = "", characters = "", newAnimation="") {
        this.root = root;
        this.url = `${url}people`;
        this.personsList = personsList;
        this.characters = characters;
        this.newAnimation = newAnimation;
    }

    request(url, id = "") {
        return fetch(`${url}/${id}`, {
            headers: {
                "content-type": "application/json",
            }
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error(response.status);
                } else {
                    return response.json();
                }
            })
            .catch(e => {
                const errorText = document.createElement("p");
                errorText.textContent = e.message;
                this.root.append(errorText);
            })
    }

    render() {
        const docFragment = document.createDocumentFragment();
        this.characters.forEach((value) => {
            const personId = value.split(`${this.url}/`)[1];
            this.request(this.url, personId)
                .then(({name}) => {
                    const personName = document.createElement("li");
                    if (name) {
                        personName.textContent = name;
                    } else {
                        personName.textContent = "name is not defined";
                    }
                    docFragment.append(personName);
                    if (this.characters.length === docFragment.childElementCount) {
                        this.personsList.append(docFragment);
                        this.newAnimation.remove();
                    }
                });
        });
    }
}

new Films(root, url, loadAnimation).render();